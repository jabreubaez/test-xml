//
//  Noticia.m
//  LectorXML
//
//  Created by Roberto Abreu on 03/02/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import "Noticia.h"

@implementation Noticia

@synthesize title,fecha,descripcion;

-(NSString *)description{
    return [NSString stringWithFormat:@"%@ %@ %@",self.title,self.fecha,self.descripcion];
}


@end
