//
//  MasterViewController.m
//  LectorXML
//
//  Created by Roberto Abreu on 03/02/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"
#import "Noticia.h"

@interface MasterViewController () {
    NSMutableArray *_objects;
    Noticia * noticia;
    NSMutableString * valorActual;
}
@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if([elementName isEqualToString:@"title"]){
        noticia.title = [valorActual stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"%@",noticia.title);
    }
    
    if([elementName isEqualToString:@"pubDate"]){
        noticia.fecha = [valorActual stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"Fecha : %@",valorActual);
    }
    
    if([elementName isEqualToString:@"description"]){
        noticia.descripcion = [valorActual stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"%@",noticia.title);
    }
    
    if([elementName isEqualToString:@"item"]){
        [_objects addObject:noticia];
    }
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    [valorActual setString:@""];
    
    if([elementName isEqualToString:@"item"]){
        noticia = [[Noticia alloc] init];
    }

}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    [valorActual appendString:string];
}

-(void)parserDidStartDocument:(NSXMLParser *)parser{
     NSLog(@"Start");
    _objects = [[NSMutableArray alloc] init];
    valorActual = [[NSMutableString alloc] init];
    
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
    NSLog(@"End");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

-(void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
     NSLog(@"Error code : %d",[parseError code]);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSURL *url = [[NSURL alloc] initWithString:@"http://feeds.feedburner.com/noticiassin1"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
       
        //Parse XML Data
        NSXMLParser * xmlParse = [[NSXMLParser alloc] initWithData:data];
        [xmlParse setDelegate:self];
        [xmlParse parse];
        
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
  
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    Noticia *object = _objects[indexPath.row];
    cell.textLabel.text = [object title];
    cell.detailTextLabel.text = [object fecha];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Noticia *object = _objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
    }
}

@end
