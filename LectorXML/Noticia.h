//
//  Noticia.h
//  LectorXML
//
//  Created by Roberto Abreu on 03/02/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Noticia : NSObject

@property (strong,nonatomic) NSString * title;
@property (strong,nonatomic) NSString * fecha;
@property (strong,nonatomic) NSString * descripcion;

@end
