//
//  MasterViewController.h
//  LectorXML
//
//  Created by Roberto Abreu on 03/02/14.
//  Copyright (c) 2014 Roberto Abreu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController <NSXMLParserDelegate>


@end
